# UMG - Fight Game
Welcome to this learning project, here you can download the whole project for free.
This projects uses the Unity HDRP.
All assets used in this project are free, and all the code is hand crafted by Gabriel Borges, no plugins were used.


## Getting started

Joystick controls:

R2 - Defense

Left Joystick - movement

Square / X - Standard hit

Triangle / Y - Hard hit

Circle / B - Long hit


Keyboard controls:

I - Defense

R2 - Defense

W,A,S,D - movement

J - Standard hit

K - Hard hit

L - Long hit


## Folders

The folders in this project are specially divided by programming pattern in order to clarify and specify the knowledge you are looking for.

## Contact me

Feel free to contact me if you want to make part of this project or have any comments/suggestions: gabriel.goncalves.borges@gmail.com