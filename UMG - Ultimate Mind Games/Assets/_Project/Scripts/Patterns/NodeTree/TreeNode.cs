using System;
using System.Collections.Generic;

[Serializable]
public struct TreeNode
{
    public List<TreeNode> Children;
}
