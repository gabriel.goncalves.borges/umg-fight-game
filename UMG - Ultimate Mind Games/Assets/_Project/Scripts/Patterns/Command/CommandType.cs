
public enum CommandType
{
    PUNCH = 0,
    PUNCH_SOFT = 1,
    PUNCH_HARD = 2,
    DEFEND = 3,
}
