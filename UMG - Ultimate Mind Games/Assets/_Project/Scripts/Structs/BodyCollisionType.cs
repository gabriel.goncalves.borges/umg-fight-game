public enum BodyCollisionType
{
   HEAD = 0,
   CHEST = 1,
   LEGS = 2,
}
